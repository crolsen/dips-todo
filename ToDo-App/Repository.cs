﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDo_App
{
    public class Repository
    {
        IFileService fs;
        Dictionary<int, ToDo> todoDictionary;

        public Repository(IFileService fs)
        {
            this.fs = fs;
            todoDictionary = new Dictionary<int, ToDo>();
            setup();
        }

        public async void setup()
        {
            var todos = await getAll();
            if (todos != null)
            {
                todoDictionary = todos;
            }
        }

        public async Task<Dictionary<int, ToDo>> getAll()
        {
            var result = await fs.readFromFile();
            return result;
        }

        public ToDo add(string task)
        {
            var newTodo = new ToDo()
            {
                id = (todoDictionary.Count != 0) ? todoDictionary.Keys.Max() + 1 : 1,
                description = task
            };
            todoDictionary.Add(newTodo.id, newTodo);
            fs.writeToFile(todoDictionary);

            return newTodo;
        }

        public ToDo remove(int id)
        {
            ToDo deletedTodo;

            if (todoDictionary.TryGetValue(id, out deletedTodo))
            {
                todoDictionary.Remove(deletedTodo.id);
                fs.writeToFile(todoDictionary);
            }

            return deletedTodo;
        }
    }
}
