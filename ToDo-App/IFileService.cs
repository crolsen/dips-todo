﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ToDo_App
{
    public interface IFileService
    {
        void writeToFile(Dictionary<int, ToDo> todoDictionary);
        Task<Dictionary<int, ToDo>> readFromFile();
    }
}
